unit app.hgapi;

interface
  uses  app.rest
        , generics.collections
        , dominio.cotacao
        , JSON;

  const HGKEY = 'dab03479';
  const HGCotacaoURL = 'https://api.hgbrasil.com/finance';

  type THGAPIApp = class
    strict private
      _server : TRESTServer;

      function getCotacao(cotacaoJSON : TJSONObject) : TCotacao;
      function JSONParaCotacoes(cotacaoJSON : TJSONObject) : TObjectList<TCotacao>;


    public
      constructor create;
      destructor free;

      function getCotacoes : TObjectList<TCotacao>;

  end;

implementation

{ THGAPIApp }

uses SysUtils
      ,strUtils
      ,utils.funcoes
      ,dominio.tipos.tipocotacao;

constructor THGAPIApp.create;
begin
  self._server := TRestServer.Create;
end;

destructor THGAPIApp.free;
begin
  self._server.Free;
end;

function THGAPIApp.getCotacao(cotacaoJSON: TJSONObject): TCotacao;
var tipo : string;
begin
  try
    Result := TCotacao.Create;
    tipo := cotacaoJSON.get('name').JsonValue.Value;
    if (AnsiUpperCase(tipo) = 'DOLLAR') then
    begin
      Result.Tipo := TTipoCotacao.tipoCotacaoDolar;
    end
    else if (AnsiUpperCase(tipo) = 'EURO') then
    begin
      Result.Tipo := TTipoCotacao.tipoCotacaoEuro;
    end
    else if AnsiUpperCase(tipo) = 'MERCADO BITCOIN' then
    begin
      Result.Tipo := TTipoCotacao.tipoCotacaoBitCoin;
    end;

    Result.Data := now;
    Result.ValorCompra := utils.funcoes.StrJSONParaFloat(cotacaoJSON.Get('buy').JsonValue.Value);
    Result.ValorVenda := utils.funcoes.StrJSONParaFloat(cotacaoJSON.Get('sell').JsonValue.Value);
    Result.Variacao := utils.funcoes.StrJSONParaFloat(cotacaoJSON.Get('variation').JsonValue.Value);
    Exit(Result);



  finally

  end;

end;

function THGAPIApp.getCotacoes: TObjectList<TCotacao>;
var resposta : TRestResposta;
begin
  try
    resposta := self._server.get(HGCotacaoURL+'?key='+HGKEY);
    if resposta.status <> 200 then
    begin
      Exit (TObjectList<TCotacao>.Create);
    end;
    result := self.JSONParaCotacoes(resposta.jsonResposta);
  finally
    FreeAndNil(resposta);
  end;
end;

function THGAPIApp.JSONParaCotacoes(cotacaoJSON: TJSONObject): TObjectList<TCotacao>;
var subObj : TJSONObject;
begin
  try
    result := TOBjectList<TCotacao>.Create;

    subObj := cotacaoJSON.Get('results').JsonValue as TJSONObject;
    subObj := subObj.Get('currencies').JsonValue as TJSONObject;
    result.Add(self.getCotacao(subObj.Get('USD').JsonValue as TJSONObject));
    result.Add(self.getCotacao(subObj.Get('EUR').JsonValue as TJSONObject));

    subObj := cotacaoJSON.Get('results').JsonValue as TJSONObject;
    subObj := subObj.Get('bitcoin').JsonValue as TJSONObject;

    result.Add(self.getCotacao(subObj.Get('mercadobitcoin').JsonValue as TJSONObject));
  except
    Result := TOBjectList<TCotacao>.Create;
  end;
end;

end.
