# alterData

Saldo bancário Alterdata

Estrutura das pastas:
/

--/app
    Arquivos de aplicação
    
--/dominio
    Arquivos de domínio, entidades
    
--/repositorios
    Comunicação com o banco de dados
    
--/utils
    Funçoes gerais
    
--/tests
    Test cases automatizados
    
--/Arquitetura
    Documentação do projeto
