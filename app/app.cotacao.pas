unit app.cotacao;

interface
  uses  dominio.cotacao
        ,app.hgapi
        ,generics.collections
        , repositorios.cotacao
        ;
  type TCotacaoApp = class
    strict private
      _HGAPIApp : THGAPIApp;
    public
      constructor create;
      destructor free;

      procedure testarConexao;

      function recuperarCotacoesAtivas : TObjectList<TCotacao>;
  end;

implementation

{ TCotacaoApp }

uses SysUtils;

constructor TCotacaoApp.create;
begin
  self._HGAPIApp := THGAPIApp.create();
end;

destructor TCotacaoApp.free;
begin
  self._HGAPIApp.free();
end;

function TCotacaoApp.recuperarCotacoesAtivas: TObjectList<TCotacao>;
var repositoriocotacao : TRepositorioCotacao;
    listaBase : TObjectList<TCotacao>;
    listaWS : TObjectList<TCotacao>;
    i: integer;
    iguais : boolean;
begin
  try
    repositoriocotacao := TRepositorioCotacao.create;
    listaBase := repositoriocotacao.recuperarUltimasCotacoes;
    listaws := self._HGAPIApp.getCotacoes();
    iguais := false;
    if (listaws.Count = 3) and (listaBase.Count = 3) then
    begin
      iguais := true;
      for I := 0 to 2 do
          if listaws[i].Tipo = listabase[i].tipo then
          begin
            iguais := iguais and listabase[i].comparar(listaws[i]);
          end;
    end;

    if (not iguais) and (listaws.Count =3) then
    begin
      repositoriocotacao.inserir(listaws[0]);
      repositoriocotacao.inserir(listaws[1]);
      repositoriocotacao.inserir(listaws[2]);
      result := listaws;
      listabase.Free;
    end
    else
    begin
      result := listabase;
      listaws.Free;
    end;
  finally
    FreeAndNil(repositoriocotacao);
  end;

end;

procedure TCotacaoApp.testarConexao;
var repositorioCotacao : TRepositorioCotacao;
begin
  repositorioCotacao := TRepositorioCotacao.create; // esse passo ir� realizar a conex�o com o banco e
                                                    // retornar exce��o em caso de erro
  repositorioCotacao.free;
end;

end.

