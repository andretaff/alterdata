unit utils.funcoes;

interface
  uses sysUtils;

  function StrJSONParaFloat(valor : string): extended;
  function CompararExtendeds(valor1, valor2 : extended; casasDepoisDaVirgula : integer=6) : boolean;
  function StringToDate(formato, str : string) : TDate;

implementation

function StrJSONParaFloat(valor : string): extended;
begin
  valor := StringReplace (valor, '.',FormatSettings.DecimalSeparator, [rfReplaceAll]);
   valor := StringReplace (valor, ',',FormatSettings.DecimalSeparator, [rfReplaceAll]);
  Result := StrToFloat(valor);
end;

function CompararExtendeds(valor1, valor2 : extended; casasDepoisDaVirgula : integer=6) : boolean;
var comparador : extended;
begin
  comparador := 1;
  while (casasDepoisDaVirgula>0) do
  begin
    comparador := comparador/10;
    Dec(casasDepoisDaVirgula);
  end;
  Result := (valor1+comparador>valor2) and (valor2+comparador>valor1);
end;

function StringToDate(formato, str : string) : TDate;
var form : TFormatSettings;
begin
  form := TFormatSettings.Create;
  form.ShortDateFormat := formato;
  form.DateSeparator :='/';
  Result := StrToDate(str,form);
end;

end.
