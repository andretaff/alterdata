unit repositorios.conexao;

interface
  uses FireDAC.comp.client
        ,FireDac.dapt
        ,Firedac.stan.Async
        ,FireDAC.stan.def;

  type TConexao = class
    private
      _conexao : TFDConnection;
      class var instancia : TConexao;

    public
      constructor create;
      destructor free;

      class function obterInstancia : TConexao;

      property Conexao : TFDConnection read _conexao;
  end;

implementation

{ TConexao }
uses IniFiles;

constructor TConexao.create;
var arquivoIni : TIniFile;
begin
  arquivoIni := TIniFile.Create('SaldoBancario.ini');
  self._conexao := TFDConnection.Create(nil);
  self._conexao.Params.Clear;
  self._conexao.Params.Add('DriverID=MySQL');
  self._conexao.Params.Add('Server=127.0.0.1');
  self._conexao.Params.Add('Port=3306');
  self._conexao.Params.Add('Database='+arquivoini.ReadString('Banco','Database','alterdata'));
  self._conexao.Params.Add('CharacterSet=utf8');
  self._conexao.Params.Add('User_Name='+arquivoini.ReadString('Banco','Ususario','root'));
  self._conexao.Params.Add('Password='+arquivoini.ReadString('Banco','Senha','iris'));

  arquivoIni.Free;

  self._conexao.Connected := True;
end;

destructor TConexao.free;
begin
  self._conexao.Free;
end;

class function TConexao.obterInstancia: TConexao;
begin
  if not Assigned(instancia) then
  begin
    instancia := TConexao.create;
  end;
 Result := instancia;
end;

end.
