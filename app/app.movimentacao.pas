unit app.movimentacao;

interface
  uses Classes
        , FireDac.comp.client
        , dominio.movimentacao
        , generics.collections
        ,app.leitorOFX;

  type TStatusProcesso = (statusAguardando,statusProcessando,statusTerminado);


  type TMovimentacaoApp = class(TThread)

    private
      _arquivo : string;
      _memTable : TFDMemTable;
      _status : TStatusProcesso;
      _importacaoOk : boolean;
      _mensagem : string;
      _movimentacoesACarregar : TObjectList<TMovimentacao>;
      _saldo : currency;

      procedure carregarOFX(arquivo : string);
      procedure carregarListaParaMemTable;

      procedure atualizarSaldo;



    protected
       procedure Execute; override;

    public
      constructor create (memTable : TFDMemTable);

      procedure iniciarCargaOFX(arquivo : string);
      procedure carregarMovimentacoes;

      property Mensagem : string read _mensagem;
      property Status : TStatusProcesso read _status;
      property ImportacaoOk : boolean read _importacaook;
      property Saldo : currency read _saldo;
  end;

implementation

{ TMovimentacaoApp }

uses repositorios.movimentacao
      ,sysutils;


procedure TMovimentacaoApp.atualizarSaldo;
begin
  _saldo := 0;
  _memTable.DisableControls;
  _memTable.First;
  while not _memTable.Eof do
  begin
    _saldo := _saldo + _memTable.FieldByName('VALOR').AsCurrency;
    _memTable.Next;
  end;

  _memTable.EnableControls;
end;

procedure TMovimentacaoApp.carregarListaParaMemTable;
var movimentacao : TMovimentacao;
begin
  for movimentacao in _movimentacoesACarregar do
    begin
      _memTable.Append;
      _memTable.FieldByName('DATA').AsDateTime := movimentacao.Data;
      _memTable.FieldByName('VALOR').AsCurrency := movimentacao.Valor;
      _memTable.FieldByName('DESCRICAO').AsString := movimentacao.Descricao;
      _memTable.Post;
    end;
end;

procedure TMovimentacaoApp.carregarMovimentacoes;
var repositorioMovimentacao : TRepositorioMovimentacao;
    listaCarga : TObjectList<TMovimentacao>;
begin
  try
    repositorioMovimentacao := TRepositorioMovimentacao.create;
    listaCarga := repositorioMovimentacao.recuperarMovimentacoes;
    _movimentacoesACarregar := listaCarga;
    self.carregarListaParaMemTable;
    self.atualizarSaldo;
  finally
    FreeAndNil(repositorioMovimentacao);
    FreeAndNil(listaCarga);
  end;
end;

procedure TMovimentacaoApp.carregarOFX(arquivo: string);
var listaCarga : TObjectList<TMovimentacao>;
    repositorioMovimentacao : TRepositorioMovimentacao;
    movimentacaoSalva : TMovimentacao;
begin
  try
    repositorioMovimentacao := nil;
    listaCarga := TleitorOFX.lerArquivoOFX(arquivo);
    repositorioMovimentacao := TRepositorioMovimentacao.Create;

    if listaCarga.Count>0 then
    begin
      movimentacaoSalva := repositorioMovimentacao.buscarPorFITID(listaCarga[0].FItid);

      if movimentacaoSalva = nil then
      begin
        repositorioMovimentacao.inserirLista(listaCarga);
        _movimentacoesACarregar := listaCarga;
        Synchronize(carregarListaParaMemTable);
        self.atualizarSaldo;
        self._importacaoOk := True;
      end
      else
      begin
        self._mensagem := 'Movimenta��o j� importada para o banco de dados';
        self._importacaoOk := False;
        movimentacaoSalva.Free;
      end;
    end
    else
    begin
      self._importacaoOk := False;
      self._mensagem := 'Houve um erro na leitura do arquivo OFX';
    end;
  finally
    FreeAndNil(listaCarga);
    FreeAndNil(repositorioMovimentacao);
  end;

end;

constructor TMovimentacaoApp.create(memTable: TFDMemTable);
begin
  inherited Create(True);
  self._memTable := memTable;
  self._arquivo := '';
end;

procedure TMovimentacaoApp.Execute;
begin
  inherited;
  while not self.Terminated do
  begin
    Sleep(100);
    if _arquivo <> '' then
    begin
      self._status := statusProcessando;
      try
        carregarOFX(_arquivo);
      except
        self._mensagem := 'Erro ao carregar arquivo';
        self._importacaoOk := False;
      end;
      self._status := statusTerminado;
      _arquivo := '';
    end;
  end;



end;

procedure TMovimentacaoApp.iniciarCargaOFX(arquivo: string);
begin
  _status := statusAguardando;
  self._arquivo := arquivo;
end;

end.
