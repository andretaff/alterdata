unit OFXFile;

interface
  uses generics.collections
        ,classes;

  type TOFXNode = class
    private
      _nome : string;
      _valor : string;
      _filhos :TObjectList<TOFXNode>;

    public
      constructor create;
      destructor free;

      function buscarFilho(nome:string): TOFXNode;

      property Nome : string read _nome write _nome;
      property Valor : string read _valor write _valor;
      property Filhos : TObjectList<TOFXNode> read _filhos;
  end;

  type TOFXFile = class
    private
      _raiz : TOFXNode;

      function lerNoValor (novalor : string) : TOFXNode;
      function lerNo(arquivo : TStrings; var indice : integer) : TOFXNode;

      function preprocessar(arquivo:TStrings):TstringList;

    public
      constructor create;
      destructor free;
      procedure lerArquivo(arquivo : string);

      property OFXNode : TOFXNode read _raiz;
  end;

implementation

uses sysutils;

{ TOFXNode }

function TOFXNode.buscarFilho(nome: string): TOFXNode;
var i : integer;
begin
  result := nil;
  for I := 0 to _filhos.Count -1 do
  begin
    if _filhos[i]._nome = nome then
    begin
      Result := filhos[i];
      exit;
    end;
  end;
end;

constructor TOFXNode.create;
begin
  _nome := '';
  _valor := '';
  _filhos := TObjectList<TOFXNode>.Create;
end;

destructor TOFXNode.free;
begin
  FreeAndNil(_filhos);
end;

{ TOFXFile }

constructor TOFXFile.create;
begin
  self._raiz := nil;
end;

destructor TOFXFile.free;
begin
  FreeAndNil(_raiz);
end;

procedure TOFXFile.lerArquivo(arquivo: string);
var arquivoLido : TstringList;
    indice : integer;
    arquivoPreProcessado : TStrings;
begin
  arquivoLido := TStringList.Create;
  arquivolido.LoadFromFile(arquivo);
  arquivoPreProcessado := self.preprocessar(arquivolido);

  indice := 0;
   while (TriM(arquivoPreProcessado.Strings[indice])<>'<OFX>') do
  begin
    inc(indice);
    if indice>= arquivoPreProcessado.Count then
    begin
      raise Exception.Create('Erro ao ler arquivo');
    end;

  end;

  try
    _raiz := self.lerNo(arquivoPreProcessado,indice);
  except
    on e:exception do
    begin
      arquivoLido.Free;
      arquivoPreProcessado.Free;
      _raiz := nil;
      raise e;
    end;
  end;
  arquivoLido.Free;
  arquivoPreProcessado.Free;


end;

function TOFXFile.lerNo(arquivo: TStrings; var indice: integer): TOFXNode;
var stringAux : string;
begin
  result := TOFXNode.create;
  stringAux := Trim(arquivo.Strings[indice]);
  result.Nome := Copy(stringAux,2,Length(stringAux)-2);
  inc(indice);
  if indice>=arquivo.Count then
  begin
    raise Exception.Create('Erro ao ler arquivo');
  end;
  while Trim(arquivo.Strings[indice]) <> '</'+result.Nome+'>' do
  begin
    stringAux := Trim(arquivo.Strings[indice]);
    if stringaux[Length(stringAux)]='>' then
    begin
      result.Filhos.Add(self.lerNo(arquivo,indice));
    end
    else
    begin
      result.Filhos.Add(self.lerNoValor(stringaux));
    end;
    inc(indice);
    if indice>=arquivo.Count then
    begin
      raise Exception.Create('Erro ao ler arquivo');
    end;
  end;
end;

function TOFXFile.lerNoValor(novalor: string): TOFXNode;
begin
  result := TOFXNode.create;
  result.Nome := Copy(novalor,2,Pos('>',novalor)-2);
  result.Valor := Copy(novalor,Pos('>',novalor)+1,length(novalor));
end;

function TOFXFile.preprocessar(arquivo: TStrings): TstringList;
var i,posicao : integer;
    strpronta, corte : string;
begin
  result := TstringList.Create;
  for i := 0 to arquivo.Count-1 do
  begin
    strpronta := Trim(arquivo.Strings[i]);
    corte := copy(strpronta,2,Length(strpronta));
    posicao := pos('<',corte);
    if posicao<>0 then
    begin
      result.Add(Copy(strpronta,1,posicao));
      if posicao=pos('>',strpronta)  then
      begin
        result.Add(copy(strpronta,posicao+1,Length(strpronta)));
      end;
    end
    else
    begin
      result.Add(strpronta);
    end;

  end;

end;

end.
