unit repositorios.cotacao;

interface
  uses repositorios.base
        ,generics.collections
        ,firedac.stan.param
        ,dominio.cotacao;

  type TRepositorioCotacao = class(TRepositorioBase<TCotacao>)
  protected
      function popularDaQuery : TCotacao; override;

    public
      function recuperarUltimasCotacoes : TOBjectList<TCotacao>;
      procedure inserir(cotacao : TCotacao); override;

  end;

implementation

{ TRepositorioCotacao }

uses dominio.tipos.tipocotacao;

procedure TRepositorioCotacao.inserir(cotacao: TCotacao);
begin
  inherited;
  _query.SQL.Text := ' INSERT INTO COTACAO ( DATA ' +
                     '                        ,TIPO ' +
                     '                        , VALORCOMPRA ' +
                     '                        , VALORVENDA  ' +
                     '                        , VARIACAO  ) ' +
                     '  VALUES            ( :DATA           ' +
                     '                      ,:TIPO          ' +
                     '                      ,:VALORCOMPRA   ' +
                     '                      , :VALORVENDA   ' +
                     '                      , :VARIACAO )  ';
  _query.ParamByName('DATA').AsDateTime := cotacao.Data;
  _query.ParamByName('TIPO').AsInteger := Ord(cotacao.Tipo);
  _query.ParamByName('VALORCOMPRA').AsExtended := cotacao.ValorCompra;
  _query.ParamByName('VALORVENDA').AsExtended := cotacao.ValorVenda;
  _query.ParamByName('VARIACAO').AsExtended := cotacao.Variacao;
  _query.ExecSQL;

end;

function TRepositorioCotacao.popularDaQuery: TCotacao;
var cotacao : TCotacao;
begin
  cotacao := TCotacao.Create;
  cotacao.Data := _query.FieldByName('Data').asDateTime;
  cotacao.Tipo := TTipoCotacao(_query.FieldByName('Tipo').asInteger);
  cotacao.ValorVenda := _query.FieldByName('VALORVENDA').AsExtended;
  cotacao.ValorCompra := _query.FieldByName('VALORCOMPRA').AsExtended;
  cotacao.variacao := _query.FieldByName('VARIACAO').AsExtended;
  Result := cotacao;

end;


function TRepositorioCotacao.recuperarUltimasCotacoes: TOBjectList<TCotacao>;
begin
  _query.sql.text := ' SELECT  ID         ' +
                     '        ,DATA       ' +
                     '        ,VALORCOMPRA' +
                     '        ,VALORVENDA ' +
                     '        ,TIPO       ' +
                     '        ,VARIACAO   ' +
                     '  FROM COTACAO     ' +
                     '  ORDER BY DATA DESC,ID ASC LIMIT 3';
  Result := self.popularLista(_query.sql.text);
end;

end.
