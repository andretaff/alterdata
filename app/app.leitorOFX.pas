unit app.leitorOFX;

interface
  uses  OFXFile
        ,generics.collections
        ,dominio.movimentacao;

  type TLeitorOFX = class
    class function lerArquivoOFX(arquivo : string) : TObjectList<TMovimentacao>;
  end;

implementation

{ TLeitorOFX }
uses utils.funcoes
      ,sysutils;

class function TLeitorOFX.lerArquivoOFX(arquivo: string): TObjectList<TMovimentacao>;
var arquivoOFX :TOFXFile;
    movimentacao : TMovimentacao;
    listaMovs : TObjectList<TOfxnode>;
    movimentoNode : TOFXNode;
    item : TOFXNode;
    strAux : string;
begin
  try
    arquivoOFX := TOFXFile.Create;
    Result := TObjectList<TMovimentacao>.Create;

    arquivoOFX.lerArquivo(arquivo);

    listaMovs:= arquivoOFX.OFXNode.buscarFilho('BANKMSGSRSV1').buscarFilho('STMTTRNRS').buscarFilho('STMTRS').buscarFilho('BANKTRANLIST').Filhos;

    for movimentonode in listamovs do
    begin
      if movimentonode.Nome='STMTTRN' then
      begin
        movimentacao := TMovimentacao.Create;
        for item in movimentonode.Filhos do
        begin
          if item.Nome = 'FITID' then
          begin
            movimentacao.FITID := item.Valor;
          end
          else if item.Nome = 'MEMO' then
          begin
            movimentacao.Descricao := item.Valor;
          end
          else if item.Nome = 'DTPOSTED' then
          begin
            strAux := item.Valor;
            strAux := Copy(straux,1,8);
            straux := Copy(straux,7,2)+'/'+Copy(straux,5,2)+'/'+Copy(straux,1,4);

            movimentacao.Data := utils.funcoes.StringToDate('dd/mm/yyyy',straux);
          end
          else if item.Nome = 'TRNAMT' then
          begin
            movimentacao.Valor := utils.funcoes.StrJSONParaFloat(item.Valor);
          end;
        end;
        result.Add(movimentacao);
      end;

    end;
  finally
    FreeAndNil(arquivoOFX);
  end;
end;

end.
