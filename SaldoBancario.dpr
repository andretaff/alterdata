program SaldoBancario;

uses
  Vcl.Forms,
  SaldoBancarioUnit in 'SaldoBancarioUnit.pas' {SaldoBancarioForm},
  app.cotacao in 'app\app.cotacao.pas',
  dominio.cotacao in 'dominio\dominio.cotacao.pas',
  dominio.tipos.tipocotacao in 'dominio\dominio.tipos.tipocotacao.pas',
  app.rest in 'app\app.rest.pas',
  app.hgapi in 'app\app.hgapi.pas',
  utils.funcoes in 'utils\utils.funcoes.pas',
  repositorios.base in 'repositorios\repositorios.base.pas',
  repositorios.conexao in 'repositorios\repositorios.conexao.pas',
  repositorios.cotacao in 'repositorios\repositorios.cotacao.pas',
  dominio.movimentacao in 'dominio\dominio.movimentacao.pas',
  app.movimentacao in 'app\app.movimentacao.pas',
  app.leitorOFX in 'app\app.leitorOFX.pas',
  OFXFile in 'utils\OFXFile.pas',
  repositorios.movimentacao in 'repositorios\repositorios.movimentacao.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TSaldoBancarioForm, SaldoBancarioForm);
  Application.Run;
end.
