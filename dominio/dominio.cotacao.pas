unit dominio.cotacao;

interface

  uses dominio.tipos.tipocotacao
        , utils.funcoes;

  type TCotacao = class
    strict private
      _data : TDateTime;
      _tipo : TTipoCotacao;
      _valorcompra : extended;
      _valorvenda : extended;
      _variacao : extended;

      function getTextoTipo : string;

    public
      function comparar(cotacao : TCotacao) : boolean;


      property Data : TDateTime read _data write _data;
      property Tipo : TTipoCotacao read _tipo write _tipo;
      property DescricaoTipo : string read getTextoTipo;
      property ValorCompra : extended read _valorcompra write _valorcompra;
      property ValorVenda : extended read _valorvenda write _valorvenda;
      property Variacao : extended read _variacao write _variacao;

  end;

implementation

{ TCotacao }

function TCotacao.comparar(cotacao: TCotacao): boolean;
begin
  Result := (self._tipo = cotacao.Tipo)
            and compararExtendeds(self._valorcompra,cotacao._valorcompra)
            and compararExtendeds(self._valorvenda,cotacao.ValorVenda)
            and compararExtendeds(self.Variacao, cotacao.Variacao);
end;

function TCotacao.getTextoTipo: string;
begin
  case self._tipo of
    tipoCotacaoDolar:
      result := 'Dolar';
    tipoCotacaoEuro:
      result := 'Euro';
    tipoCotacaoBitCoin:
      result := 'BitCoin';
  end;
end;

end.
