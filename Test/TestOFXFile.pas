unit TestOFXFile;
{

  Delphi DUnit Test Case
  ----------------------
  This unit contains a skeleton test case class generated by the Test Case Wizard.
  Modify the generated code to correctly setup and call the methods from the unit
  being tested.

}

interface

uses
  TestFramework, classes, generics.collections, OFXFile;

type
  // Test methods for class TOFXFile

  TestTOFXFile = class(TTestCase)
  strict private
    FOFXFile: TOFXFile;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestlerArquivo;
  end;

implementation

procedure TestTOFXFile.SetUp;
begin
  FOFXFile := TOFXFile.Create;
end;

procedure TestTOFXFile.TearDown;
begin
  FOFXFile.Free;
  FOFXFile := nil;
end;

procedure TestTOFXFile.TestlerArquivo;
var
  arquivo: string;
begin
  arquivo := 'ofxok.ofx';
  FOFXFile.lerArquivo(arquivo);
  Assert(FOFXFile.OFXNode.buscarFilho('SIGNONMSGSRSV1')<>nil);
  Assert(FOFXFile.OFXNode.buscarFilho('SIGNONMSGSRSV1').buscarFilho('SONRS').buscarFilho('STATUS').buscarFilho('CODE').Valor='0');
end;

initialization
  // Register any test cases with the test runner
  RegisterTest(TestTOFXFile.Suite);
end.

