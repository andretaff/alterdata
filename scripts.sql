CREATE DATABASE alterdata;

USE ALTERDATA;

CREATE TABLE COTACAO (
					ID INT NOT NULL AUTO_INCREMENT,
                    TIPO smallint,
                    DATA datetime,
                    VALORCOMPRA double,
                    VALORVENDA DOUBLE,
                    VARIACAO DOUBLE,
                    PRIMARY KEY (ID));



CREATE TABLE MOVIMENTACAO (
	ID INTEGER NOT NULL auto_increment,
    FITID VARCHAR(50),
    DATA DATE,
    VALOR float,
    DESCRICAO VARCHAR(50),	
    PRIMARY KEY(ID));

