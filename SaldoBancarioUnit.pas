unit SaldoBancarioUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls
  ,app.cotacao
  ,app.movimentacao
  ,dominio.cotacao
  ,dominio.tipos.tipocotacao, FireDAC.Phys.MySQLDef, FireDAC.Stan.Intf,
  FireDAC.Phys, FireDAC.Phys.MySQL, FireDAC.UI.Intf, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error,
  FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls;

type
  TSaldoBancarioForm = class(TForm)
    gbSaldoBancario: TGroupBox;
    gbImportarArquivoOFX: TGroupBox;
    gbCotacao: TGroupBox;
    reCotacaoDolar: TRichEdit;
    reCotacaoEuro: TRichEdit;
    reCotacaoBitcoin: TRichEdit;
    btAtualizarCotacoes: TButton;
    lbCotacao: TLabel;
    lbDiaCotacao: TLabel;
    driverLink: TFDPhysMySQLDriverLink;
    fdWaitCursor: TFDGUIxWaitCursor;
    mtMovimentacoes: TFDMemTable;
    gbCabecalhoMovimentos: TGroupBox;
    lbMovimentacao: TLabel;
    grdMovimentacao: TDBGrid;
    dsMovimentacaoes: TDataSource;
    mtMovimentacoesdata: TDateField;
    mtMovimentacoesDescricao: TStringField;
    btImportarArquivoOFX: TButton;
    OpenOFX: TOpenDialog;
    TimerOFX: TTimer;
    mtMovimentacoesValor: TFloatField;
    lbSaldo: TLabel;
    lbSaldoValor: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btAtualizarCotacoesClick(Sender: TObject);
    procedure btImportarArquivoOFXClick(Sender: TObject);
    procedure TimerOFXTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    _cotacaoApp : TCotacaoApp;
    _movimentacaoApp : TMovimentacaoApp;

    procedure limparEdits;
    procedure atualizarSaldo;
    procedure  logarErro(msg : string);
    procedure atualizarEditCotacao(edit : TRichEdit; cotacao : TCotacao);

  public
    { Public declarations }
  end;

var
  SaldoBancarioForm: TSaldoBancarioForm;

implementation

{$R *.dfm}

uses generics.collections;

procedure TSaldoBancarioForm.atualizarEditCotacao(edit: TRichEdit;
  cotacao: TCotacao);
begin
  edit.lines.Add(cotacao.DescricaoTipo);
  edit.Lines.Add('   Venda:  R$'+FormatFloat('0.00',cotacao.ValorVenda));
  edit.Lines.Add('   Compra: R$'+FormatFloat('0.00',cotacao.ValorCompra));
  edit.Lines.Add('   Varia��o: '+FormatFloat('0.000%',cotacao.Variacao));
end;

procedure TSaldoBancarioForm.atualizarSaldo;
begin
  if _movimentacaoApp.Saldo>=0 then
  begin
    lbSaldoValor.Caption := FormatFloat('R$ #0.00',_movimentacaoApp.Saldo);
    lbSaldoValor.Font.Color := clCaptionText;
  end
  else
  begin
    lbSaldoValor.Caption := FormatFloat('R$ #0.00 (-)',-_movimentacaoApp.Saldo);
    lbSaldoValor.Font.Color := clred;
  end;
end;

procedure TSaldoBancarioForm.btAtualizarCotacoesClick(Sender: TObject);
var cotacoes : TObjectList<TCotacao>;
begin
  try
    cotacoes := self._cotacaoApp.recuperarCotacoesAtivas();
  except
    on e:Exception do
    begin
      logarErro('Erro ao recuperar cota��es: '+ e.Message);
      Exit;
    end;

  end;
  if (cotacoes <> nil) and (cotacoes.Count = 3) then
  begin
    self.limparEdits;
    self.atualizarEditCotacao(reCotacaoDolar,cotacoes[0]);
    self.atualizarEditCotacao(reCotacaoEuro,cotacoes[1]);
    self.atualizarEditCotacao(reCotacaoBitcoin,cotacoes[2]);
    lbDiaCotacao.Caption := FormatDateTime('(dd/mm/yyyy)',cotacoes[0].Data);
  end
  else
  begin
    logarErro('Erro ao recuperar cota��es, tente novamente');
  end;
  cotacoes.Free;
end;

procedure TSaldoBancarioForm.btImportarArquivoOFXClick(Sender: TObject);
begin
  if ( (_movimentacaoApp.status = statusAguardando) or (_movimentacaoApp.status = statusTerminado) )
      and OpenOFX.Execute then
  begin
    _movimentacaoApp.iniciarCargaOFX(OPenOFX.FileName);
    TimerOFX.Enabled := True;
  end;
end;

procedure TSaldoBancarioForm.FormCreate(Sender: TObject);
begin
  mtMovimentacoes.Active := True;
  self._cotacaoApp := TCotacaoApp.create;
  self._movimentacaoApp := TMovimentacaoApp.create(mtMovimentacoes);
  self._movimentacaoApp.Start;
  try
    _cotacaoApp.testarConexao;
  except
    on e: Exception do
    begin
      logarErro('Houve um erro ao conectar com o banco de dados: '+e.Message);
    end;
  end;
end;

procedure TSaldoBancarioForm.FormDestroy(Sender: TObject);
begin
  self._cotacaoApp.free;
  self._movimentacaoApp.Terminate;
  self._movimentacaoApp.Free;
end;

procedure TSaldoBancarioForm.FormShow(Sender: TObject);
begin
  self.limparEdits;
  self._movimentacaoApp.carregarMovimentacoes;
  self.atualizarSaldo;
end;

procedure TSaldoBancarioForm.limparEdits;
begin
  reCotacaoDolar.Lines.Clear;
  reCotacaoEuro.Lines.Clear;
  reCotacaoBitcoin.Lines.Clear;
  lbDiaCotacao.Caption := '';
end;

procedure TSaldoBancarioForm.logarErro(msg: string);
begin
  ShowMessage(msg);
end;

procedure TSaldoBancarioForm.TimerOFXTimer(Sender: TObject);
begin
  if _movimentacaoApp.status = statusTerminado then
  begin
    TimerOFX.Enabled := False;
    self.atualizarSaldo;
    if _movimentacaoApp.importacaook then
    begin
      ShowMessage('Arquivo '+OpenOFX.FileName+' importado com sucesso');
    end
    else
    begin
      LogarErro(_movimentacaoApp.mensagem);
    end;
  end;
end;

end.
