unit dominio.movimentacao;

interface
  type TMovimentacao = class
    strict private
      _fitid : string;
      _descricao : string;
      _valor : currency;
      _data : TDate;

    public
      property FITID : string read _fitid write _fitid;
      property Descricao : string read _descricao write _descricao;
      property Valor : currency read _valor write _valor;
      property Data : TDate read _data write _data;

  end;

implementation

end.
