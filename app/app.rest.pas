unit app.rest;

interface
   uses   IdTCPClient
         ,IdHTTP
         ,JSon;

   type TRESTResposta = class
      private
         _status : integer;
         _resposta : TJSONObject;
         _mensagem : string;
         _url : string;
      public
        constructor create;
        destructor free;


        property status : integer read _status write _status;
        property jsonResposta : TJSONObject read _resposta write _resposta;
        property mensagem : string read _mensagem write _mensagem;
        property url : string read _url write _url;
   end;

   type TRESTServer = class
      private
         _server: TIdHTTP;

      public

         constructor Create;
         destructor Free;

         function get(url : string):TRESTResposta;

   end;

implementation

{ }
uses   Classes
      ,SysUtils;

constructor TRESTServer.Create;
begin
   self._server := TIdHTTP.Create;
   self._server.Request.UserAgent := 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0';
   self._server.Request.BasicAuthentication := False;
   self._server.Request.ContentType         := 'application/json';
   self._server.Request.AcceptCharSet       := 'application/json';
   self._server.Request.Accept              := 'application/json';
   self._server.Request.Accept := '*/*';
end;

destructor TRESTServer.Free;
begin
   self._server.Free;
end;

function TRESTServer.get(url: string): TRESTResposta;
var
   retornoEnvio: TStringStream;
begin
   result := TRESTResposta.Create;
   retornoEnvio := TStringStream.Create('',TEncoding.UTF8);
   try
      result.url := url;
      _server.Get(url,retornoEnvio);
      result.jsonResposta := TJSONObject.ParseJSONValue(retornoEnvio.DataString) as TJSONObject;
      result.status := 200;
      result.mensagem := 'OK';

   except
      on e:EIdHTTPProtocolException  do
      begin
         result.status := e.ErrorCode;
         result.mensagem := e.Message;
      end;
      on e:Exception do
      begin
         result.status := 404;
         result.mensagem := 'ERRO AO ACESSAR ENDPOINT';
      end;
   end;
   retornoEnvio.Free;
end;
{ TRESTResposta }

constructor TRESTResposta.create;
begin
  self._resposta := TJSONObject.Create;
end;

destructor TRESTResposta.free;
begin
  self._resposta.Free;
end;

end.
