program SaldoBancarioTests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  TestappREST in 'TestappREST.pas',
  app.rest in '..\app\app.rest.pas',
  DUnitTestRunner,
  TestOFXFile in 'TestOFXFile.pas',
  OFXFile in '..\utils\OFXFile.pas' {R *.RES},
  TestappLeitorOFX in 'TestappLeitorOFX.pas',
  dominio.movimentacao in '..\dominio\dominio.movimentacao.pas',
  utils.funcoes in '..\utils\utils.funcoes.pas',
  app.leitorOFX in '..\app\app.leitorOFX.pas';

{R *.RES}

begin
  DUnitTestRunner.RunRegisteredTests;
end.

