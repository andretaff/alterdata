unit repositorios.movimentacao;

interface
  uses repositorios.base
        ,generics.collections
        ,dominio.movimentacao;

  type TRepositorioMovimentacao = class(TRepositorioBase<TMovimentacao>)
    protected
        function popularDaQuery : TMovimentacao;  override;

      public
        function recuperarMovimentacoes : TOBjectList<TMovimentacao>;
        procedure inserir(movimentacao : TMovimentacao); override;
        procedure inserirLista( lista : TObjectList<TMovimentacao>);
        function buscarPorFITID(fitid : string) : TMovimentacao;
  end;

implementation

{ TRepositorioMovimentacao }
uses sysutils
      ,FireDac.Stan.Param;

function TRepositorioMovimentacao.buscarPorFITID(fitid: string) : TMovimentacao;
begin
  Result := self.popularUnico('SELECT FITID, DATA, VALOR, DESCRICAO FROM MOVIMENTACAO WHERE FITID = '''+(fitid)+'''');
end;

procedure TRepositorioMovimentacao.inserir(movimentacao: TMovimentacao);
begin
  inherited;
  _query.SQL.Text := ' INSERT INTO MOVIMENTACAO (   FITID      ' +
                     '                              ,DATA     ' +
                     '                              ,VALOR    ' +
                     '                              ,DESCRICAO )' +
                     '              VALUES       ( :FiTID       ' +
                     '                             ,:DATA      ' +
                     '                             ,:VALOR     ' +
                     '                             ,:DESCRICAO)';
  _query.ParamByName('FITID').AsString := movimentacao.FITID;
  _query.ParamByName('DATA').AsDate := movimentacao.Data;
  _query.ParamByName('VALOR').AsFloat := movimentacao.Valor;
  _query.ParamByName('DESCRICAO').AsString := movimentacao.Descricao;
  _query.ExecSQL;
end;

procedure TRepositorioMovimentacao.inserirLista(
  lista: TObjectList<TMovimentacao>);
var movimentacao : TMovimentacao;
begin
  for movimentacao in lista do
  begin
    self.inserir(movimentacao);
  end;
end;

function TRepositorioMovimentacao.popularDaQuery: TMovimentacao;
begin
  result := TMovimentacao.Create;
  result.FITID := _query.FieldByName('FITID').AsString;
  result.Descricao := _query.FieldByName('DESCRICAO').AsString;
  result.Valor := _query.FieldByName('VALOR').AsCurrency;
  result.Data := _query.FieldByName('DATA').AsDateTime;
end;

function TRepositorioMovimentacao.recuperarMovimentacoes: TObjectList<TMovimentacao>;
begin
  Result := self.popularLista(' SELECT    FITID,     ' +
                              '           DATA,     ' +
                              '           VALOR,    ' +
                              '           DESCRICAO ' +
                              ' FROM MOVIMENTACAO ORDER BY DATA ');
end;

end.
