object SaldoBancarioForm: TSaldoBancarioForm
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Consulta financeira'
  ClientHeight = 596
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbSaldoBancario: TGroupBox
    Left = 0
    Top = 0
    Width = 800
    Height = 394
    Align = alClient
    TabOrder = 0
    object gbImportarArquivoOFX: TGroupBox
      Left = 2
      Top = 326
      Width = 796
      Height = 66
      Align = alBottom
      TabOrder = 0
      object lbSaldo: TLabel
        Left = 537
        Top = 16
        Width = 52
        Height = 18
        Caption = 'Saldo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object lbSaldoValor: TLabel
        Left = 595
        Top = 16
        Width = 41
        Height = 18
        Caption = 'Valor'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object btImportarArquivoOFX: TButton
        Left = 9
        Top = 11
        Width = 152
        Height = 33
        Caption = 'Importar arquivo Ofx'
        TabOrder = 0
        OnClick = btImportarArquivoOFXClick
      end
    end
    object gbCabecalhoMovimentos: TGroupBox
      Left = 2
      Top = 15
      Width = 796
      Height = 26
      Align = alTop
      TabOrder = 1
      object lbMovimentacao: TLabel
        Left = 9
        Top = 3
        Width = 192
        Height = 18
        Caption = 'Movimenta'#231#227'o banc'#225'ria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
    end
    object grdMovimentacao: TDBGrid
      Left = 2
      Top = 41
      Width = 796
      Height = 285
      Align = alClient
      DataSource = dsMovimentacaoes
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ReadOnly = True
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'data'
          Title.Caption = 'Data'
          Width = 110
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 463
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 187
          Visible = True
        end>
    end
  end
  object gbCotacao: TGroupBox
    Left = 0
    Top = 394
    Width = 800
    Height = 202
    Align = alBottom
    TabOrder = 1
    object lbCotacao: TLabel
      Left = 11
      Top = 16
      Width = 138
      Height = 18
      Caption = 'Cota'#231#245'es do dia '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object lbDiaCotacao: TLabel
      Left = 155
      Top = 16
      Width = 39
      Height = 18
      Caption = '(dia)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object reCotacaoDolar: TRichEdit
      Left = 11
      Top = 56
      Width = 238
      Height = 121
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        'reCotacaoDolar')
      ParentFont = False
      TabOrder = 0
      Zoom = 100
    end
    object reCotacaoEuro: TRichEdit
      Left = 275
      Top = 56
      Width = 238
      Height = 121
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        'reCotacaoDolar')
      ParentFont = False
      TabOrder = 1
      Zoom = 100
    end
    object reCotacaoBitcoin: TRichEdit
      Left = 539
      Top = 56
      Width = 238
      Height = 121
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        'reCotacaoDolar')
      ParentFont = False
      TabOrder = 2
      Zoom = 100
    end
    object btAtualizarCotacoes: TButton
      Left = 632
      Top = 6
      Width = 145
      Height = 35
      Caption = 'Atualizar cota'#231#245'es'
      TabOrder = 3
      OnClick = btAtualizarCotacoesClick
    end
  end
  object driverLink: TFDPhysMySQLDriverLink
    Left = 736
    Top = 200
  end
  object fdWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 650
    Top = 227
  end
  object mtMovimentacoes: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 592
    Top = 232
    object mtMovimentacoesdata: TDateField
      FieldName = 'data'
    end
    object mtMovimentacoesDescricao: TStringField
      FieldName = 'Descricao'
      Size = 50
    end
    object mtMovimentacoesValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = 'R$#.00;R$#.00 (-)'
    end
  end
  object dsMovimentacaoes: TDataSource
    DataSet = mtMovimentacoes
    Left = 514
    Top = 179
  end
  object OpenOFX: TOpenDialog
    Filter = 'Arquivo OFX|*.ofx'
    Left = 474
    Top = 206
  end
  object TimerOFX: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TimerOFXTimer
    Left = 464
    Top = 274
  end
end
