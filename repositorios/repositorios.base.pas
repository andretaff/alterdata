unit repositorios.base;

interface
  uses FireDac.Comp.Client
        ,generics.collections
        ,repositorios.conexao;

  type TRepositorioBase<T:class> = class abstract
    protected
      _query : TFDQuery;


      function popularDaQuery : T; virtual; abstract;
      function popularUnico (sql : string) : T; virtual;
      function popularLista (sql : string) : TOBjectList<T>; virtual;

    public
      constructor create;
      destructor free;

      procedure inserir (objeto : T); virtual; abstract;


  end;

implementation

{ TRepositorioBase<T> }

constructor TRepositorioBase<T>.create;
begin
  self._query := TFDQuery.Create(nil);
  self._query.Connection := TConexao.obterInstancia.Conexao;
end;

destructor TRepositorioBase<T>.free;
begin
  self._query.Free;
end;

function TRepositorioBase<T>.popularLista(sql: string): TOBjectList<T>;
begin
  Result := TObjectList<T>.Create;
  self._query.close;
  self._query.SQL.Text := sql;
  self._query.open();
  while not _query.eof do
  begin
    Result.add(self.popularDaQuery);
    _query.next;
  end;
end;

function TRepositorioBase<T>.popularUnico(sql: string): T;
begin
  self._query.close;
  self._query.SQL.Text := sql;
  self._query.open();
  if not _query.eof then
  begin
    Result := self.popularDaQuery;
  end
  else
  begin
    Result := nil;
  end;
end;

end.
